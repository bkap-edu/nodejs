const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var course = [
    {id: 1, name:'angular'},
    {id: 2, name:'nodejs'},
    {id: 3, name:'php'},
    {id: 4, name:'laravel'}
]

app.get('/', (req, res) => {
    // res.send(JSON.stringify(course));
    res.json(course);
});


app.get('/chat', (req, res) => {
    // res.send(JSON.stringify(course));
    res.sendFile(__dirname + '/index.html');
});

app.post('/', (req, res) => {

    if(req.body.id){
        console.log(req.body);

        course.push(req.body);
        res.send('success');
    }else{
        res.status(500).send('body not null')
    }
});



app.listen(3000, '127.0.0.1', () => {
    console.log('server runing')
});