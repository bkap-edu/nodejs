const db = require('../utils/dbconnection');
const tbl_name = 'product';

module.exports = {
    getAll: (callback) => {
        return db.query(`SELECT * FROM ${tbl_name}`, callback);
    },
    getById: (id, callback) => {
        return db.query(`SELECT * FROM ${tbl_name} WHERE id=?`, [id], callback);
    },
    insert: (entity, callback) => {
        return db.query(
            `INSERT INTO ${tbl_name}(category_id, name, description, price, image, status) VALUES(?,?,?,?,?,?)`, 
            [entity.category_id,entity.name,entity.description,entity.price,entity.image,entity.status], 
            callback
        );
    },
    update: (id, entity, callback) => {
        if(entity.image != undefined && entity.image != ''){
            return db.query(
                `UPDATE ${tbl_name} SET category_id=?, name=?, description=?, price=?, image=?, status=? WHERE id=?`,
                [entity.category_id,entity.name, entity.description, entity.price,entity.image, entity.status, id],
                callback
            );
        }else{
            return db.query(
                `UPDATE ${tbl_name} SET category_id=?, name=?, description=?, price=?, status=? WHERE id=?`,
                [entity.category_id,entity.name, entity.description, entity.price, entity.status, id],
                callback
            );
        }
       
    },
    delete: (id, callback) => {
        return db.query(
            `DELETE FROM ${tbl_name} WHERE id=?`,
            [id],
            callback
        )
    }
}