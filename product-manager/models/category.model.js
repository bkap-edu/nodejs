const db = require('../utils/dbconnection');

const Category = {
    getAll: function(callback){
        return db.query("SELECT * FROM category", callback);
    },
    getById: function(id, callback){
        return db.query("SELECT * FROM category WHERE id=?", [id], callback);
    },
    insert: function(entity, callback){
        if(entity.image != undefined && entity.image != ''){
            return db.query("INSERT INTO category(name, description,image) VALUES(?,?,?)",[entity.name, entity.description, entity.image], callback);
        }
        return db.query("INSERT INTO category(name, description) VALUES(?,?)",[entity.name, entity.description], callback);
    },
    update: function(id, entity, callback){
        if(entity.image != undefined && entity.image != ''){
            return db.query("UPDATE category set name=?, description=?, image=? WHERE id=?", [entity.name, entity.description, entity.image, id], callback);
        }
        return db.query("UPDATE category set name=?, description=? WHERE id=?", [entity.name, entity.description, id], callback);
    },
    delete: function(id, callback){
        return db.query("DELETE FROM category WHERE id=?", [id], callback);
    }
}

module.exports = Category;