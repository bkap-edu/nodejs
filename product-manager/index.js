const express = require('express'),
        bodyParser = require('body-parser'),
        path = require('path'),
        mysql = require('mysql');

// Khởi tạo module express
const app = express();

global.__basedir = __dirname;


app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
// app.use('/assets', express.static(__dirname +'/assets'));
app.use('/',express.static(__dirname +'/public'));

// Khai báo sử dụng middleware để convert dữ liệu  dạng json
app.use(bodyParser.json());

// Khai báo sử dụng midleware để convert dữ liệu chứa trong url
app.use(bodyParser.urlencoded({extended: false}));

// app.get('/', (req, res) => {
//     res.sendStatus(200);
// });

app.get('/', (req, res) => {
    res.render('home',{});
});

const category_router = require('./routes/category.router');
const product_router = require('./routes/product.router');
app.use('/category',category_router);
app.use('/product',product_router);

// Gọi phương thức lắng nghe client

const server = app.listen(3000, '127.0.0.1', () => {
    console.log(`Server running http://${server.address().address}:${server.address().port}`);
});
