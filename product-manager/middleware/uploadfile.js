/**
 * 1- Import module multer
 * 2- Cấu hình multer sử dụng Disk storage
 * 3- Định nghĩa bộ lọc chỉ cho tệp hình ảnh qua
 * 
 */

const multer = require('multer');
const { update } = require('../models/category.model');

const imageFilter = (req, file, callback) => {
    if(file.mimetype.startsWith('image')){
        callback(null, true);
    }else{
        callback('Yêu cầu chọn ảnh', false);
    }
};

var storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, __basedir + "/public/uploads/");
    },
    filename: (req, file, callback) => {
        callback(null, `${Date.now()}-${file.originalname}`);
        
    }
});

var uploadfile = multer({storage: storage, fileFilter: imageFilter});

module.exports = uploadfile;
