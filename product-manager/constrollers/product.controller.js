const model = require('../models/product.model');
const modelCategory = require('../models/category.model');
const { end } = require('../utils/dbconnection');

module.exports = {
    index: function(req, res, next) {
        model.getAll((err, rows) => {
            if(err) {res.send(err);}
            else{
                console.log(rows);
                res.render('product/list', {categories: rows});
            }
        });
    },
    
    form: function(req, res, next) {
        let id = req.params.id;
        
        modelCategory.getAll((err, categories)=> {
            if(categories === undefined){
                
            }else{
                if(id !=undefined){
                    model.getById(id, (err, data) => {
                        if(err){
                            console.log(err);
                        }else{
                            console.log(data);
                            res.render('product/form', { entity: data[0], categories: categories});
                        }
                    });
                }else{
                    res.render('product/form', { entity: {}, categories: categories});
                }

            }
        });
        
       
    },
    save: function(req, res, next) {
       let id = req.body.id;
       let filename = '';
       if(req.file != undefined){
            filename = "/uploads/" + req.file.filename;
            console.log(filename);
            req.body.image=filename;
       }
       if(id){
        
        model.update(id, req.body, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log(data);
            }
        });
       }else{
        model.insert(req.body, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log(data);
            }
        });
       }
        res.redirect('/product');
    },
    delete: (req, res, next) => {
        model.delete(req.params.id, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log(data);
            }
        });
        res.redirect('/product');
    }
}