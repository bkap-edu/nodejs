const categoryModel = require('../models/category.model');

module.exports = {
    index: function(req, res, next) {
        categoryModel.getAll((err, rows) => {
            if(err) {res.send(err);}
            else{
                console.log(rows);
                res.render('category/list', {categories: rows});
            }
        });
    },
    
    form: function(req, res, next) {
        let id = req.params.id;
     
        if(id !=undefined){
            categoryModel.getById(id, (err, data) => {
                if(err){
                    console.log(err);
                }else{
                    console.log(data);
                    res.render('category/form', { entity: data[0]});
                }
            });
        }else{
            res.render('category/form', { entity: {}});
        }
       
    },
    save: function(req, res, next) {
       let id = req.body.id;
       let filename = '';
       if(req.file != undefined){
            filename = "/uploads/" + req.file.filename;
            console.log(filename);
            req.body.image=filename;
       }
       if(id){
        categoryModel.update(id, req.body, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log(data);
            }
        });
       }else{
        categoryModel.insert(req.body, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log(data);
            }
        });
       }
        res.redirect('/category');
    },
    delete: (req, res, next) => {
        categoryModel.delete(req.params.id, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log(data);
            }
        });
        res.redirect('/category');
    }
}