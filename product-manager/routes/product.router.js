const express = require('express');
const upload = require('../middleware/uploadfile');

const router = express.Router();

const controller = require('../constrollers/product.controller');

router.get('/', controller.index);

router.get('/form', controller.form);
router.get('/form/:id', controller.form);
// Nếu upload nhiều file thì upload.array('ten', so_luong)
router.post('/save', upload.single('fileimage'), controller.save);
router.get('/delete/:id', controller.delete);

module.exports = router;