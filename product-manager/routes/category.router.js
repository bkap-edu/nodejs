const express = require('express');
const upload = require('../middleware/uploadfile');

const category_router = express.Router();

const category_controller = require('../constrollers/category.controller');

category_router.get('/', category_controller.index);

category_router.get('/form', category_controller.form);
category_router.get('/form/:id', category_controller.form);
// Nếu upload nhiều file thì upload.array('ten', so_luong)
category_router.post('/save', upload.single('fileimage'), category_controller.save);
category_router.get('/delete/:id', category_controller.delete);

module.exports = category_router;