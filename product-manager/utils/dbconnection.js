const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    database: 'api_node',
    user: 'root',
    password: '',
    charset: 'utf8'
});

module.exports = connection;