const http = require('http');
const url = require('url');

const server = http.createServer((request, response) => {
    let q = url.parse(request.url, true).query;
    // response.setHeader('Content-Type', 'text/plain');
    if(q.name === undefined || q.age === undefined){
        response.write('Require name + age in url');
        response.end();
    }
    let message = `${q.name} is ${q.age} years old`;
    response.write(message);
    response.end();
});


server.listen(3000, ()=> {
 console.log("server running 3000");
});