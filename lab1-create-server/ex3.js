hello =function(){
    console.log('hello world');
}

// Khai bao hang kieu float 
const PI = 3.14;

// Khai bao lop 

class Rectange {
    constructor(length, width){
        this.length = length;
        this.width = width;
    }

    showInfo(){
        console.log(`Rectange ${this.width} x ${this.length}: `);
    }

    showArea(){
        console.log(`Area: ${this.width * this.length}`);
    }

    showCircuit() {
        console.log(`Circuit: ${2*(this.length+ this.width)}`);
    }
}

module.exports = {
    hello: hello,
    PI: PI,
    Rectange: Rectange,
};