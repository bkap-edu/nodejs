const http = require('http');

const fs = require('fs');

const server = http.createServer((request, response) => {
    const url = request.url;
    const method = request.method;
    // response.setHeader('Content-Type', 'text/html');
    switch(url){
        case '/':
           
            // response.write("hi");
            response.write("<html>");
            response.write("<head>");
            response.write("<title>Enter message");
            response.write("</title>");
            response.write("<head>");
            response.write("<body>");
            response.write("<form method='POST' action='/message' >");
            response.write("<input type='text' name='message' />");
            response.write("<input type='submit' value='send' />");
            response.write("</form>");
            response.write("</body>");
            response.write("</html>");
            response.end();
        break;

        case '/message': 
            let body = [];
            if(method === 'POST'){
                request.on('data', (chunk) => {
                    console.log(chunk);
                    body.push(chunk);
                });
                request.on('end', () => {
                    let parseBody = Buffer.concat(body).toString();
                    console.log(parseBody);
                    let message = parseBody.split('=')[1];
                    fs.writeFileSync('message.txt', message);
                });

                response.statusCode = 302;
                response.setHeader('Location', '/');
                return response.end();
            }
            response.write("message page");
            response.end();
        break;
    }
});

server.listen(3000);