const express = require('express');

const mysql =require('mysql');
const bodyParser = require('body-parser');

const app = express();

const connection = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    database: 'api_node',
    user: 'root',
    password: ''
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get('/list', function(req, res) {

   connection.query('SELECT * FROM customers', function(err, rows){
       res.json(rows);
   });

});

app.post('/', function(req, res){
    console.log(req.body);
    res.end();
});

app.put('/:id', function(req, res){
res.send(req.body)
})

app.delete('/:id', function(req, res){
    res.send(req.params)
})

let home_route = require('./home-route')(app);


app.listen(3000, () =>  {
    console.log('server running');
});