module.exports = (app) => {
    const express = require('express');
    const router = express.Router();

    router.get('/', (req, res) => {
        console.log('test home');
        res.send(req.url);
    });

    app.use('/api/home', router);
}