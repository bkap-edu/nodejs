import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { ContactService } from "../../services/contact.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
})
export class ContactFormComponent implements OnInit {
  formDetail: FormGroup;
  uuid='';
  constructor(
    private service:ContactService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.formDetail = new FormGroup({
      uuid: new FormControl(''),
      contactTitle: new FormControl(''),
      firstName: new FormControl(''),
      middleName: new FormControl(''),
      lastName: new FormControl(''),
      leadReferralSource: new FormControl(''),
      title: new FormControl(''),
      company: new FormControl(''),
      industry: new FormControl(''),
      address1: new FormControl(''),
      address2: new FormControl(''),
      phone: new FormControl(''),
      email: new FormControl(''),
      status: new FormControl(''),
      website: new FormControl(''),
      rating: new FormControl(''),
    });

    this.uuid = this.route.snapshot.queryParams.uuid||'';
    console.log(this.route.snapshot.queryParams.uuid);
    if(this.uuid != ''){
      this.getOne(this.uuid);
    }

  }

  getOne(uuid:string){
    this.service.getById(uuid).subscribe((res:any) => {
      this.formDetail.setValue({
        uuid: res.uuid,
        contactTitle: res.contactTitle,
        firstName: res.firstName,
        middleName: res.middleName,
        lastName: res.lastName,
        leadReferralSource: res.leadReferralSource,
        title: res.title,
        company: res.company,
        industry: res.industry,
        address1: res.address1,
        address2: res.address2,
        phone: res.phone,
        email: res.email,
        status: res.status,
        website: res.website,
        rating: res.rating,
      });
      })
  }


  onSubmit(){
    console.log('onSubmit');
    if(this.uuid != ''){
      this.service.update(this.uuid, this.formDetail.value).subscribe();
    }else{
      this.service.create(this.formDetail.value).subscribe();
    }
    this.router.navigate(['contact']);
  }

}
