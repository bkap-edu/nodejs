import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { TaskStatusService } from "../../services/task-status.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-status-form',
  templateUrl: './task-status-form.component.html',
  styleUrls: ['./task-status-form.component.css']
})
export class TaskStatusFormComponent implements OnInit {
  formDetail: FormGroup;
  id=0;
  constructor(
    private service:TaskStatusService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.formDetail = new FormGroup({
      id: new FormControl(''),
      status: new FormControl('')
    });

    this.id = this.route.snapshot.queryParams.id||0;
    // console.log(this.route.snapshot.queryParams.id);
    if(this.id>0){
      this.getOne(this.id);
    }

  }

  getOne(id:number){
    this.service.getById(id).subscribe((res:any) => {
      this.formDetail.setValue({
        id: res.id,
        status: res.status
      })
    })
  }


  onSubmit(){
    console.log('onSubmit');
    if(this.id > 0){
      this.service.update(this.id, this.formDetail.value).subscribe();
    }else{
      this.service.create(this.formDetail.value).subscribe();
    }
    this.router.navigate(['task-status']);
  }

}
