import { Component, OnInit,ViewChild,OnDestroy } from '@angular/core';


import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Subscription } from 'rxjs';
import { TaskStatusService } from "../../services/task-status.service";
import { TaskStatus } from 'src/app/models/task-status.model';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-task-status',
  templateUrl: './task-status.component.html',
})
export class TaskStatusComponent implements OnInit, OnDestroy {


  subscription: Subscription;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  pageSizeOptions:number[] = [5,10,25,100];


  // MatPaginator output
  pageEvent:PageEvent;

  setPageSizeOption(setPageSizeOptionsInput: string){
    if(setPageSizeOptionsInput){
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  datas: TaskStatus[]=[
    {id: 1, status:'New'}
  ];

  displayedColumns: string[] = ['id', 'status', 'function'];

  dataSource: MatTableDataSource<any>;

  constructor(private service:TaskStatusService) { }

  ngOnInit(): void {
    this.getAll();
  }
  ngOnDestroy(){
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
    }
  }

  getAll(){
    this.service.getAll().subscribe((res:any) => {
      this.datas = res;
      this.dataSource = new MatTableDataSource(this.datas);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, err => {
      console.log(err);
    });
}

delete(id:number){
  this.service.delete(id).subscribe();
}

}
