import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";

import { Subscription } from "rxjs";
import { Contact } from "../../models/contact.model";
import { ContactService } from "../../services/contact.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  pageSize = 5;
  currentPage = 0;
  totalSize = 0;
  pageSizeOptions: number[] = [5,10,25,100];

  @ViewChild(MatPaginator, {static:true})
  paginator: MatPaginator;
  @ViewChild(MatSort, {static: true})
  sort: MatSort;

  datas: Contact[]= [];
  displayedColumns: string[] = ["id", "contactTitle", "company", "function"];
  dataSource: MatTableDataSource<Contact>;

  constructor(private service: ContactService) { }

  ngOnInit(): void {
    this.getAll();
  }

  ngOnDestroy(): void{
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  applyFilter(filterValue: string){
    filterValue  = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.dataSource.filter = filterValue;
    if(this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAll(){
    this.service.getAll().subscribe(res => {
      this.datas = res;
      this.dataSource = new MatTableDataSource(this.datas);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  delete(id:string) {
    this.service.delete(id).subscribe();
  }

}
