import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from "../../services/login.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin:FormGroup;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {

    console.log(sessionStorage.getItem("token"));
    var logined = sessionStorage.getItem("token") ? true : false;
    if(logined){
      this.router.navigate(['dashboard']);
    }

    this.formLogin = this.formBuilder.group ({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }

  onSubmit(){

    this.loginService.login(this.formLogin.value).subscribe((res: any) => {

      if(res.token != undefined && res.token != ""){
        sessionStorage.setItem("token", res.token);
        sessionStorage.setItem("expiresIn", res.expiresIn);
        sessionStorage.setItem("userId", res.userId);
        this.router.navigate(['dashboard']);
      }else{
        console.log(res);
      }
    }, (err: any) => {
      console.log(err);
    });

  }


  checkLogin(){
    var logined = sessionStorage.getItem("token") ? true : false;
    if(logined){
      this.router.navigate(['dashboard']);
    }
  }
  logout(){

  }

}
