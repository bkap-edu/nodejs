import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { catchError, tap, map } from "rxjs/operators";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Contact } from "../models/contact.model";
import { baseUrl, httpOptions } from "../services/config.service";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  url:string = baseUrl + 'contact';

  constructor(private httpClient: HttpClient) {

  }

  getAll(): Observable<Contact[]>{
    return this.httpClient.get<Contact[]>(this.url,httpOptions)
    .pipe(
      tap(o => console.log(o)),
      catchError(this.handleError("getAll", []))
    );
  }

  getById(id:string):Observable<Contact>{
    return this.httpClient.get<Contact>(this.url+'/'+id,httpOptions)
    .pipe(
      tap(o => console.log(o)),
      catchError(this.handleError<Contact>('get By Id='+id))
    )
  }

  create(obj:Contact):Observable<Contact>{
    console.log(obj);
    return this.httpClient.post<Contact>(this.url, obj, httpOptions)
  }

  update(id:any, obj:Contact):Observable<Contact>{
    return this.httpClient.put<Contact>(this.url+'/'+id,obj, httpOptions).pipe()
  }

  delete(id:any):Observable<any>{
   return this.httpClient.delete(this.url, httpOptions);
  }

  private handleError<T>(operator = 'operation', result?:T){
    return (error: any) : Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
