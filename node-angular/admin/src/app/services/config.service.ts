
import { HttpHeaders } from "@angular/common/http";

export const baseUrl = 'http://localhost:85/';

//export let tokenDemo = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6bnVsbCwiaWQiOjEsImlhdCI6MTU5ODQwNTQwNCwiZXhwIjoxNTk4NDE2MjA0fQ.C_aUOYNtQzyy8G9-CUuq6dBhQtzBo04oHU_1vkGFaos';

// let tokenDemo = sessionStorage.getItem('token');

export const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${sessionStorage.getItem('token')}`
  })
};
