import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { catchError, tap, map } from "rxjs/operators";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { TaskStatus } from "../models/task-status.model";
import { baseUrl, httpOptions } from "../services/config.service";


@Injectable({
  providedIn: 'root'
})
export class TaskStatusService {


  constructor(private httpClient: HttpClient) {

  }

  getAll(): Observable<TaskStatus[]>{
    return this.httpClient.get<TaskStatus[]>(this.url,httpOptions)
    .pipe(
      tap(o => console.log(o)),
      catchError(this.handleError("getAll", []))
    );
  }

  url:string = baseUrl + 'task_status';

  getById(id:number):Observable<TaskStatus>{
    return this.httpClient.get<TaskStatus>(this.url+'/'+id,httpOptions)
    .pipe(
      tap(o => console.log(o)),
      catchError(this.handleError<TaskStatus>('get By Id='+id))
    )
  }

  create(obj:TaskStatus):Observable<TaskStatus>{
    console.log(obj);
    return this.httpClient.post<TaskStatus>(this.url, obj, httpOptions)
  }

  update(id:any, obj:TaskStatus):Observable<TaskStatus>{
    return this.httpClient.put<TaskStatus>(this.url+'/'+id,obj, httpOptions).pipe()
  }

  delete(id:any):Observable<any>{
   return this.httpClient.delete(this.url, httpOptions);
  }

  private handleError<T>(operator = 'operation', result?:T){
    return (error: any) : Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
