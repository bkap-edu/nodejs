import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable } from 'rxjs';
import { baseUrl, httpOptions } from './config.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  url = baseUrl + 'auth/login';

  login(obj: any): Observable<any>{
    return this.httpClient.post(
      this.url,
      {username: obj.username, password: obj.password},
      httpOptions
    );
  }
}
