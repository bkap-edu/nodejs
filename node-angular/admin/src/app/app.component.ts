import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router){}

  @Output() public sidenavToggle = new EventEmitter();

  title = 'admin';

  onToggleSidenav(){
    this.sidenavToggle.emit();
    console.log('humberger toggle');
  }

  logout(){
    sessionStorage.removeItem("token");
    this.router.navigate(['login']);
  }
}
