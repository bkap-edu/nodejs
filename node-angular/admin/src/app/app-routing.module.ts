import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskStatusComponent } from './components/task-status/task-status.component';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TaskStatusFormComponent } from './components/task-status-form/task-status-form.component';
import { ContactComponent } from './components/contact/contact.component';
import { ContactFormComponent } from "./components/contact-form/contact-form.component";
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from "./shared/auth.guard";


const routes: Routes = [
  {path: '*', redirectTo:'login'},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'task-status', component: TaskStatusComponent, canActivate: [AuthGuard]},
  {path: 'task-status/form', component: TaskStatusFormComponent, canActivate: [AuthGuard]},
  {path: 'contact', component: ContactComponent, canActivate: [AuthGuard]},
  {path: 'contact/form', component: ContactFormComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
