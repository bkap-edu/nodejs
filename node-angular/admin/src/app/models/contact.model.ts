export class Contact {
  uuid: string;
  contactTitle: string;
  firstName: string;
  middleName: string;
  lastName: string;
  leadReferralSource: string;
  title: string;
  company: string;
  industry: string;
  address1: string;
  address2: string;
  phone:string;
  email:string;
  status:string;
  website:string;
  rating:string;
}
