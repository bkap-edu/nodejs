import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BackendComponent } from './backend.component';
import { DashboardComponent } from '../backend/dashboard/dashboard.component';
import { ContactComponent } from '../components/contact/contact.component';
import { PostCategoryComponent } from './post-category/post-category.component';
import { PostComponent } from './post/post.component';



const routes: Routes = [{
  path: '',
  component: BackendComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'post-category',
      component: PostCategoryComponent,
    },
    {
      path: 'post',
      component: PostComponent,
    },
    {
      path: 'contact',
      component: ContactComponent,
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackendRoutingModule { }

export const routedComponents = [
  BackendComponent,
  PostCategoryComponent,
  PostComponent
];
