import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule as ngFormsModule } from '@angular/forms';

import { BackendRoutingModule,routedComponents } from './backend-routing.module';
import {
  NbMenuModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCheckboxModule,
  NbRadioModule,
  NbDatepickerModule} from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [...routedComponents],
  imports: [
    // CommonModule,
    ngFormsModule,
    NbButtonModule,
    NbActionsModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    BackendRoutingModule,
    ThemeModule,
    NbMenuModule,
    Ng2SmartTableModule
  ]
})
export class BackendModule { }
