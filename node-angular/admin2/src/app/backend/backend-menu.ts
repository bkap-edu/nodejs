import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
    {
    title: 'Dashboard',
    icon: 'shopping-cart-outline',
    link: '/backend/dashboard',
    home: true,
  },
   {
    title: 'Post',
    icon: 'layout-outline',
    children: [
      {
        title: 'Post Category',
        link: '/backend/post-category',
      },
      {
        title: 'Post',
        link: '/backend/post',
      },
    ],
  },
  {
    title: 'Contact',
    icon:'list-outline',
    link: '/backend/contact'
  },
];
