import { Component } from '@angular/core';

import { MENU_ITEMS } from './backend-menu';

@Component({
  selector: 'ngx-backend',
  template: `
  <ngx-one-column-layout>
    <nb-menu [items]="menu"></nb-menu>
    <router-outlet></router-outlet>
  </ngx-one-column-layout>
`,
  styleUrls: ['./backend.component.scss']
})
export class BackendComponent  {

  constructor() { }

  menu = MENU_ITEMS;
}
