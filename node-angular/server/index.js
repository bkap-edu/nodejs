
const PORT = 85;
const express = require('express'),
cors = require('cors');

const router = require('./routes');
const app = express();

global.TokenList = [];

app.use(express.json());
app.use(express.urlencoded({ extended:false}));
app.use(cors({
    origin: 'http://localhost:4200',

}));

// Middleware set header return client
app.use((req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();

});

app.get('/', (req, res) => {
    if(!req.body ) return res.status(404);
    res.send('welcome');
});

const sequelize = require("./utils/database");
// Tự động tạo cơ sở dữ liệu nếu chưa có
sequelize.sync({force: false}).then(() => {

});

router.load(app, './controllers');

app.listen(PORT);
