
const config = require('../config/token.config')

const jwt = require("jsonwebtoken");
// Kiểm tra hợp lệ của token
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        console.log(token);
        const decodedToken = jwt.verify(token, config.accessTokenSecret);
        req.userData = { email: decodedToken.email, userId: decodedToken.userId };
        next();
    } catch (error) {
        res.status(401).json({ message: "Auth failed!" });
        next();
    }
};
