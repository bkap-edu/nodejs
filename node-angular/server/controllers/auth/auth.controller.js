
const jwt = require("jsonwebtoken");
const tokenConfig = require('../../config/token.config');
const model = require('../../models/user.model');

class AuthController {


    constructor(router) {
        router.post('/login', this.login.bind(this));
        router.post('/refresh-token', this.refreshToken.bind(this));
    }

    async login(req, res){
// console.log(req.body);
        let user = {};
        await model.findOne({
            where: {username: req.body.username, password: req.body.password}
        }).then(data => {
            if(!data){
                return res.status(401).json({
                    message: 'Authe failed data'
                })
            }
            const token = jwt.sign(
                {email: data.email, id: data.id},
                tokenConfig.accessTokenSecret,
                {expiresIn: tokenConfig.accessTokenLife}
            );

            res.status(200).json({
                token: token,
                expiresIn: tokenConfig.accessTokenLife,
                userId: data.id
            });
        }).catch(err => {
            console.log(err)
;            res.status(401).json({
                message: 'Auth failed database'
            });
        });

    }

    async refreshToken(req, res){
// User gửi mã refresh token kèm theo trong body
        const refreshTokenFromClient = req.body.refreshToken;
        // debug("tokenList: ", tokenList);

        // Nếu như tồn tại refreshToken truyền lên và nó cũng nằm trong tokenList của chúng ta
        if (refreshTokenFromClient && (TokenList[refreshTokenFromClient])) {
            try {
                // Verify kiểm tra tính hợp lệ của cái refreshToken và lấy dữ liệu giải mã decoded
                const decoded = await jwtUtil.verifyToken(refreshTokenFromClient, tokenConfig.refreshTokenSecret);
                // Thông tin user lúc này các bạn có thể lấy thông qua biến decoded.data
                // có thể mở comment dòng debug bên dưới để xem là rõ nhé.
                // debug("decoded: ", decoded);
                const userFakeData = decoded.data;

                const accessToken = await jwtUtil.generateToken(userFakeData, tokenConfig.accessTokenSecret, tokenConfig.accessTokenLife);
                // gửi token mới về cho người dùng
                return res.status(200).json({accessToken});
            } catch (error) {
                // Lưu ý trong dự án thực tế hãy bỏ dòng debug bên dưới, mình để đây để debug lỗi cho các bạn xem thôi
                debug(error);
                res.status(403).json({
                    message: 'Invalid refresh token.',
                });
            }
        } else {
            // Không tìm thấy token trong request
            return res.status(403).send({
                message: 'No token provided.',
            });
        }
    }
}
module.exports = AuthController;
