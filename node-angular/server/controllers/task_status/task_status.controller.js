const model = require('../../models/task_status.model');

class TaskStatusController {

    constructor(router) {
        router.get('/', this.getAll.bind(this));
        router.get('/page/:skip/:top', this.getPaging.bind(this));
        router.get('/:id', this.getById.bind(this));
        router.post('/', this.insert.bind(this));
        router.put('/:id', this.update.bind(this));
        router.delete('/:id', this.delete.bind(this));
    }

    getAll(req, res){
        model.findAll({})
            .then(data => {
                res.send(data);
            })
            .catch(err => {
               res.status(500)
                   .send({message: err.message || "Lỗi truy cập cơ sở dữ liệu"})
            });
    }
    getPaging(req, res) {
        console.log('*** getPaging');
        const topVal = req.params.top,
            skipVal = req.params.skip,
            top = (isNaN(topVal)) ? 10 : +topVal,
            skip = (isNaN(skipVal)) ? 0 : +skipVal;

       model.findAll({
           limit: top,
           offset: skip
       })
           .then(data => {
            res.send(data);
        })
            .catch(err => {
                res.status(500)
                    .send({message: err.message || "Lỗi truy cập cơ sở dữ liệu"})
            });
    }

    getById(req, res) {
        console.log('*** getByID');
        const id = req.params.id;
        console.log(id);

        model.findByPk(id)
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error retrieving with id=" + id
                });
            });
    }

    insert(req, res) {
        if(!req.body.status){
            res.status(400).send({
                message: 'status không bỏ trống'
            });
            return;
        }

        const obj = {
            status: req.body.status,
        }

        model.create(obj)
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occured while creating the User."
                });
            });
    }

    update(req, res) {
        console.log('*** update');
        console.log('*** req.body');
        console.log(req.body);

        const id = req.params.id;

        model.update(req.body, {
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "Cập nhật thành công."
                    });
                } else {
                    res.send({
                        message: `Lỗi cập nhật id=${id}. Không tìm thấy dữ liệu hợp lệ`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Không thể cập nhật id=" + id
                });
            });

    }

    delete(req, res) {
        let id = req.params.id;
        model.destroy({
            where: {id: id}
        })
            .then(num => {
                if(num == 1){
                    res.send({
                        message: 'Cập nhật thành công'
                    })
                }else{
                    res.send({
                        message: `Không thể xóa dữ liệu id = ${id}`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: `Không thể xóa dữ liệu id = ${id}`
                })
            });
    }
}

module.exports = TaskStatusController;
