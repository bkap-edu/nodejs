// module.exports = {
//
//     getAll: (callback) => {
//         return {customers:[]};
//     },
//     getPaging: (skip, top, callback) => {},
//     getById: (id, callback) => {},
//     getState: (id, callback) => {},
//     insert: (id, callback) => {},
//     update: (id, callback) => {},
//     delete: (id, callback) => {}
//
// }
const { DataTypes} = require("sequelize");
const sequelize = require('../utils/database');


    const User = sequelize.define("user", {
        fullname: {
            type: DataTypes.STRING
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING
        },
        gender: {
            type: DataTypes.BOOLEAN
        },
        birthday: {
            type: DataTypes.DATE,
        }

    });


module.exports = User;
