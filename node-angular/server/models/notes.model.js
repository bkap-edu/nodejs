const DataTypes = require('sequelize');
const sequelize = require('../utils/database');

const Notes = sequelize.define('notes', {
   date: {
       type: DataTypes.DATE
   },
   notes: {
       type: DataTypes.STRING(4000)
   }
});

module.export = Notes;
