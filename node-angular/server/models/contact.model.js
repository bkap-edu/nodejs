const {Sequelize, DataTypes} = require('sequelize');
const sequelize = require('../utils/database');
const User = require('./user.model');

const Contact = sequelize.define('contact', {
    uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
    },
    contactTitle: {
        type: DataTypes.STRING(30),

    },
    firstName: {
        type: DataTypes.STRING(30),
        allowNull: false
    },
    middleName: {
        type: DataTypes.STRING(30),
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING(30),
        allowNull: false
    },
    leadReferralSource: {
        type: DataTypes.STRING(200),
        allowNull: true
    },
    title: {
        type: DataTypes.STRING(30),
        allowNull: true
    },
    company: {
        type: DataTypes.STRING(150),
        allowNull: true
    },
    industry: {
        type: DataTypes.STRING(300),
        allowNull: true
    },
    address1: {
        type: DataTypes.STRING,
        allowNull: true
    },
    address2: {
        type: DataTypes.STRING,
        allowNull: true
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: true
    },
    email: {
        type: DataTypes.STRING,
        allowNull: true
    },
    status: {
        type: DataTypes.STRING,
        allowNull: true
    },
    website: {
        type: DataTypes.STRING,
        allowNull: true
    },
    rating: {
        type: DataTypes.STRING,
        allowNull: true
    },

}, {
    freezeTableName: true
    }
    );

// Contact.hasOne(User);
// User.belongsTo(Contact);

module.exports = Contact;
