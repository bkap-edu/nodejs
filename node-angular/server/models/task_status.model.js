const DataTypes = require('sequelize');
const sequelize = require('../utils/database');

const TaskStatus = sequelize.define('task_status', {
    status: {
        type: DataTypes.STRING
    }
});

module.exports = TaskStatus;
